package com.webinfotech.grocerszone.repository.Category;

import android.util.Log;

import com.google.gson.Gson;
import com.webinfotech.grocerszone.domain.models.Category.CategoryListWrapper;
import com.webinfotech.grocerszone.domain.models.Category.SubcategoryWrapper;
import com.webinfotech.grocerszone.domain.models.ImageSliders.ImageSliderWrapper;
import com.webinfotech.grocerszone.repository.ApiClient;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Raj on 11-02-2019.
 */

public class CategoryRepositoryImpl {
    CategoryRepository mRepository;

    public CategoryRepositoryImpl() {
        mRepository = ApiClient.createService(CategoryRepository.class);
    }

    public CategoryListWrapper getCategoryList(){
        Log.e("LogMsg", "Get Category List: ");
        CategoryListWrapper categoryListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> categoryList = mRepository.getCategoryList();

            Response<ResponseBody> response = categoryList.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    categoryListWrapper = null;
                }else{
                    categoryListWrapper = gson.fromJson(responseBody, CategoryListWrapper.class);

                }
            } else {
                categoryListWrapper = null;
            }
        }catch (Exception e){
            categoryListWrapper = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return categoryListWrapper;
    }

    public ImageSliderWrapper getImageSliders(){
        ImageSliderWrapper imageSliderWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> images = mRepository.getImageSliders();

            Response<ResponseBody> response = images.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    imageSliderWrapper = null;
                }else{
                    imageSliderWrapper = gson.fromJson(responseBody, ImageSliderWrapper.class);

                }
            } else {
                imageSliderWrapper = null;
            }
        }catch (Exception e){
            imageSliderWrapper = null;
        }
        return imageSliderWrapper;
    }

    public SubcategoryWrapper getSubcategoryList(int catId) {
        SubcategoryWrapper subcategoryWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> get = mRepository.getSubcategoryList(catId);

            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    subcategoryWrapper = null;
                }else{
                    subcategoryWrapper = gson.fromJson(responseBody, SubcategoryWrapper.class);
                }
            } else {
                subcategoryWrapper = null;
            }
        }catch (Exception e){
            subcategoryWrapper = null;
        }
        return subcategoryWrapper;
    }

}
