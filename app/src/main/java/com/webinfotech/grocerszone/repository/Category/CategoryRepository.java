package com.webinfotech.grocerszone.repository.Category;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Raj on 11-02-2019.
 */

public interface CategoryRepository {

    @POST("category/category_list.php")
    Call<ResponseBody> getCategoryList();

    @POST("api/slider/sliders.php")
    Call<ResponseBody> getImageSliders();

    @GET("category/sub_category_list.php")
    Call<ResponseBody> getSubcategoryList(@Query("cat_id") int catId);

}
