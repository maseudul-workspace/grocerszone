package com.webinfotech.grocerszone.presentation.ui.activities;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.webinfotech.grocerszone.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TermsConditionActivity extends CommonBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_terms_condition);
        getSupportActionBar().setTitle("Terms and Conditions");
        ButterKnife.bind(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
