package com.webinfotech.grocerszone.presentation.presenters;

public interface LoginPresenter {
    void checkLogin(String email, String password);
    interface View {
        void onLoginSuccess();
        void showLoader();
        void hideLoader();
    }
}
