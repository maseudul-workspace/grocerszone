package com.webinfotech.grocerszone.presentation.presenters;

import com.webinfotech.grocerszone.presentation.ui.adapters.ProductListVerticalAdapter;

public interface ProductListPresenter {
    void fetchProductList(int type, int pageNo, int categoryId, String refresh);
    interface View {
        void loadProductListAdapter(ProductListVerticalAdapter productListVerticalAdapter, int totalPage);
        void goToProductDetails(int productId);
        void showLoader();
        void hideLoader();
        void showLoginSnackbar();
        void showGoToCartSnackbar();
    }
}
