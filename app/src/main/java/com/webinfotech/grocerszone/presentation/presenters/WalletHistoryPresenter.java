package com.webinfotech.grocerszone.presentation.presenters;

import com.webinfotech.grocerszone.domain.models.Wallet.WalletDetails;
import com.webinfotech.grocerszone.presentation.ui.adapters.WalletHistoryAdapter;

public interface WalletHistoryPresenter {
    void fetchWalletHistory(int page, String type);
    interface View {
        void loadAdapter(WalletHistoryAdapter adapter, WalletDetails walletDetails, int totalPage);
        void showLoader();
        void hideLoader();
    }
}
