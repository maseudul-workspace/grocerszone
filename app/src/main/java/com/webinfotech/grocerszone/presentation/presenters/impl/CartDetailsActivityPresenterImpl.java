package com.webinfotech.grocerszone.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.grocerszone.AndroidApplication;
import com.webinfotech.grocerszone.domain.executors.Executor;
import com.webinfotech.grocerszone.domain.executors.MainThread;
import com.webinfotech.grocerszone.domain.interactors.DeleteCartItemInteractor;
import com.webinfotech.grocerszone.domain.interactors.FetchShippingAddressInteractor;
import com.webinfotech.grocerszone.domain.interactors.GetCartDetailsInteractor;
import com.webinfotech.grocerszone.domain.interactors.GetWalletStatusInteractor;
import com.webinfotech.grocerszone.domain.interactors.OrderPlaceInteractor;
import com.webinfotech.grocerszone.domain.interactors.UpdateCartInteractor;
import com.webinfotech.grocerszone.domain.interactors.impl.DeleteCartItemInteractorimpl;
import com.webinfotech.grocerszone.domain.interactors.impl.FetchShippingAddressInteractorImpl;
import com.webinfotech.grocerszone.domain.interactors.impl.GetCartDetailsInteractorImpl;
import com.webinfotech.grocerszone.domain.interactors.impl.GetWalletStatusInteractorImpl;
import com.webinfotech.grocerszone.domain.interactors.impl.OrderPlaceInteractorImpl;
import com.webinfotech.grocerszone.domain.interactors.impl.UpdateCartInteractorImpl;
import com.webinfotech.grocerszone.domain.models.Cart.Cart;
import com.webinfotech.grocerszone.domain.models.User.ShippingAddress;
import com.webinfotech.grocerszone.domain.models.User.UserInfo;
import com.webinfotech.grocerszone.domain.models.Wallet.WalletStatus;
import com.webinfotech.grocerszone.presentation.presenters.CartDetailsActivityPresenter;
import com.webinfotech.grocerszone.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.grocerszone.presentation.ui.adapters.CartAdapter;
import com.webinfotech.grocerszone.presentation.ui.adapters.CartShippingAddressAdapter;
import com.webinfotech.grocerszone.repository.ProductRepository.impl.CartRepositoryImpl;
import com.webinfotech.grocerszone.repository.User.UserRepositoryImpl;

import es.dmoral.toasty.Toasty;

/**
 * Created by Raj on 10-01-2019.
 */

public class CartDetailsActivityPresenterImpl extends AbstractPresenter implements  CartDetailsActivityPresenter,
                                                                                    GetCartDetailsInteractor.Callback,
                                                                                    DeleteCartItemInteractor.Callback,
                                                                                    UpdateCartInteractor.Callback,
                                                                                    CartAdapter.Callback,
                                                                                    FetchShippingAddressInteractor.Callback,
                                                                                    CartShippingAddressAdapter.Callback,
                                                                                    OrderPlaceInteractor.Callback,
                                                                                    GetWalletStatusInteractor.Callback
                                                                                    {

    Context mContext;
    AndroidApplication androidApplication;
    GetCartDetailsInteractorImpl mCartDetailsInteractor;
    int userId;
    CartDetailsActivityPresenter.View mView;
    CartAdapter adapter;
    UpdateCartInteractorImpl updateCartInteractor;
    DeleteCartItemInteractorimpl deleteCartItemInteractorimpl;
    FetchShippingAddressInteractorImpl fetchShippingAddressInteractor;
    CartShippingAddressAdapter cartShippingAddressAdapter;
    ShippingAddress[] addresses;
    int addressId;
    OrderPlaceInteractorImpl orderPlaceInteractor;
    GetWalletStatusInteractorImpl getWalletStatusInteractor;
    int walletStatus = 1;

    public CartDetailsActivityPresenterImpl(Executor executor,
                                            MainThread mainThread,
                                            Context context,
                                            CartDetailsActivityPresenter.View view) {
        super(executor, mainThread);
        this.mContext = context;
        this.mView = view;
    }

    @Override
    public void getCartList() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            mCartDetailsInteractor = new GetCartDetailsInteractorImpl(mExecutor,
                    mMainThread,
                    new CartRepositoryImpl(),
                    this,
                    userInfo.userId,
                    userInfo.apiKey);
            mCartDetailsInteractor.execute();
            mView.showLoadingProgress();
        } else {
            Toasty.warning(mContext, "Your Session Is Expired!! Please Login Again").show();
        }
    }

    @Override
    public void fetchShippingAddress() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            fetchShippingAddressInteractor = new FetchShippingAddressInteractorImpl(mExecutor, mMainThread, new UserRepositoryImpl(), this, userInfo.userId, userInfo.apiKey);
            fetchShippingAddressInteractor.execute();
        } else {
            Toasty.warning(mContext, "Your Session Is Expired!! Please Login Again").show();
        }
    }

    @Override
    public void fetchWalletAmount() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            getWalletStatusInteractor = new GetWalletStatusInteractorImpl(mExecutor, mMainThread, this, new UserRepositoryImpl(), userInfo.apiKey, userInfo.userId);
            getWalletStatusInteractor.execute();
        } else {
            Toasty.warning(mContext, "Your Session Is Expired!! Please Login Again").show();
        }
    }

    @Override
    public void placeOrder() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            orderPlaceInteractor = new OrderPlaceInteractorImpl(mExecutor, mMainThread, this, new UserRepositoryImpl(), userInfo.apiKey, userInfo.userId, walletStatus, addressId);
            orderPlaceInteractor.execute();
            mView.showLoadingProgress();
        } else {
            Toasty.warning(mContext, "Your Session Is Expired!! Please Login Again").show();
        }
    }

    @Override
    public void updateCart(int cartId, int quantity) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            updateCartInteractor = new UpdateCartInteractorImpl(mExecutor, mMainThread, new CartRepositoryImpl(), this, quantity, userInfo.userId, cartId, userInfo.apiKey);
            updateCartInteractor.execute();
            mView.showLoadingProgress();
        } else {
            Toasty.warning(mContext, "Your Session Is Expired!! Please Login Again").show();
        }
    }

    @Override
    public void deleteCart(int cartId) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            deleteCartItemInteractorimpl = new DeleteCartItemInteractorimpl(mExecutor, mMainThread, new CartRepositoryImpl(), this, userInfo.userId, cartId, userInfo.apiKey);
            deleteCartItemInteractorimpl.execute();
            mView.showLoadingProgress();
        } else {
            Toasty.warning(mContext, "Your Session Is Expired!! Please Login Again").show();
        }
    }

    @Override
    public void onGetCartDetailsSuccess(Cart[] carts) {
        if (carts.length == 0) {
            mView.hideViews();
            mView.hideLoadingProgress();
        } else {
            double subTotal = 0;
            double discount = 0;
            double grandTotal = 0;
            for (int i = 0; i < carts.length; i++) {
                subTotal = subTotal + carts[i].cartQty * carts[i].mrp;
                discount = discount + carts[i].cartQty * (carts[i].mrp - carts[i].price);
                grandTotal = grandTotal + carts[i].cartQty * carts[i].price;
            }
            adapter = new CartAdapter(mContext,  carts, this);
            mView.loadCartItemList(adapter, subTotal, discount, grandTotal);
            mView.hideLoadingProgress();
        }
    }

    @Override
    public void onGetCartDetailsFail(String errorMsg) {
        mView.hideLoadingProgress();
        mView.hideViews();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onUpdateCartSuccess(String successMsg) {
        getCartList();
        Toasty.success(mContext, successMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUpdateCartFail(String errorMsg) {
        mView.hideLoadingProgress();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCartItemDeletedSuccess(String successMsg) {
        getCartList();
        Toasty.success(mContext, successMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCartItemDeleteFail(String errorMsg) {
        mView.hideLoadingProgress();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAddressFetchSuccess(ShippingAddress[] shippingAddresses) {
        this.addresses = shippingAddresses;
        cartShippingAddressAdapter = new CartShippingAddressAdapter(mContext, shippingAddresses, this);
        mView.loadAddressAdapter(cartShippingAddressAdapter);
    }

    @Override
    public void onAddressFetchFail(String errorMsg) {

    }

    @Override
    public void onAddressSelected(int id) {
        this.addressId = id;
        for (int i = 0; i < addresses.length; i++) {
            if (addresses[i].id == id) {
                addresses[i].isSelected = true;
            } else {
                addresses[i].isSelected = false;
            }
        }
        cartShippingAddressAdapter.updateDataSet(addresses);
    }

    @Override
    public void onEditClicked(int id) {

    }

    @Override
    public void onDeliverButtonClicked() {
        mView.onDeliverButtonClicked();
    }

    @Override
    public void onOrderPlacedSuccess() {
        mView.hideLoadingProgress();
        mView.hideViews();
        Toasty.success(mContext, "Order Placed Successfully", Toast.LENGTH_SHORT).show();
        mView.goToOrderHistory();
    }

    @Override
    public void onOrderPlacedFail(String errorMsg) {
        mView.hideLoadingProgress();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGettingWalletStatusSuccess(WalletStatus walletStatus) {
    }

    @Override
    public void onGettingWalletStatusFail(String errorMsg) {

    }
}
