package com.webinfotech.grocerszone.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;

import com.google.android.material.textfield.TextInputLayout;
import com.webinfotech.grocerszone.R;
import com.webinfotech.grocerszone.domain.executors.impl.ThreadExecutor;
import com.webinfotech.grocerszone.presentation.presenters.LoginPresenter;
import com.webinfotech.grocerszone.presentation.presenters.impl.LoginPresenterImpl;
import com.webinfotech.grocerszone.threading.MainThreadImpl;

public class LoginActivity extends AppCompatActivity implements LoginPresenter.View {

    LoginPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.edit_text_password)
    EditText editTextPassword;
    @BindView(R.id.txt_input_email_layout)
    TextInputLayout textInputEmailLayout;
    @BindView(R.id.txt_input_password_layout)
    TextInputLayout textInputLayoutPasswordLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
    }

    private void initialisePresenter() {
        mPresenter = new LoginPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @OnClick(R.id.btn_log_in) void onLoginClicked() {
        textInputEmailLayout.setError("");
        textInputLayoutPasswordLayout.setError("");
        if (editTextEmail.getText().toString().trim().isEmpty()) {
            textInputEmailLayout.setError("Email Required");
            textInputEmailLayout.requestFocus();
        } else if (editTextPassword.getText().toString().trim().isEmpty()) {
            textInputLayoutPasswordLayout.setError("Password Required");
            textInputLayoutPasswordLayout.requestFocus();
        } else {
            mPresenter.checkLogin(editTextEmail.getText().toString(), editTextPassword.getText().toString());
            showLoader();
        }
    }

    @OnClick(R.id.layout_register) void onRegisterClicked() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    @Override
    public void onLoginSuccess() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @OnClick(R.id.txt_view_forget_password) void onForgetPasswordClicked() {
        Intent intent = new Intent(this, ForgotPasswordActivity.class);
        startActivity(intent);
    }
}
