package com.webinfotech.grocerszone.presentation.presenters;

import com.webinfotech.grocerszone.presentation.presenters.base.BasePresenter;
import com.webinfotech.grocerszone.presentation.ui.adapters.CartAdapter;
import com.webinfotech.grocerszone.presentation.ui.adapters.CartShippingAddressAdapter;

/**
 * Created by Raj on 10-01-2019.
 */

public interface CartDetailsActivityPresenter extends BasePresenter {
    void getCartList();
    void fetchShippingAddress();
    void fetchWalletAmount();
    void placeOrder();
    interface View{
        void loadCartItemList(CartAdapter adapter, double subTotal, double discount, double grandTotal);
        void loadAddressAdapter(CartShippingAddressAdapter cartShippingAddressAdapter);
        void onDeliverButtonClicked();
        void goToOrderHistory();
        void hideViews();
        void showLoadingProgress();
        void hideLoadingProgress();
    }
}
