package com.webinfotech.grocerszone.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.grocerszone.R;
import com.webinfotech.grocerszone.domain.models.Category.Category;
import com.webinfotech.grocerszone.util.GlideHelper;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainCategoriesAdapter extends RecyclerView.Adapter<MainCategoriesAdapter.ViewHolder> {

    public interface Callback {
        void onCategoryClicked(int catId, String categoryName, boolean subcategoryStatus);
    }

    Context mContext;
    Category[] categories;
    Callback mCallback;

    public MainCategoriesAdapter(Context mContext, Category[] categories, Callback callback) {
        this.mContext = mContext;
        this.categories = categories;
        this.mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_categories, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GlideHelper.setImageView(mContext, holder.imgViewCategory, mContext.getResources().getString(R.string.base_url) + "/uploads/main_category/thumb/" + categories[position].image);
        holder.txtViewCategoryName.setText(categories[position].category);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onCategoryClicked(categories[position].id, categories[position].category, categories[position].subCategoryStatus);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categories.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.img_view_category)
        ImageView imgViewCategory;
        @BindView(R.id.txt_view_category_name)
        TextView txtViewCategoryName;
        @BindView(R.id.main_layout)
        View mainLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

}
