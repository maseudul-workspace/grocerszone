package com.webinfotech.grocerszone.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;

import com.webinfotech.grocerszone.R;
import com.webinfotech.grocerszone.domain.executors.impl.ThreadExecutor;
import com.webinfotech.grocerszone.presentation.presenters.DownlineActivityPresenter;
import com.webinfotech.grocerszone.presentation.presenters.impl.DownlineActivityPresenterImpl;
import com.webinfotech.grocerszone.presentation.ui.adapters.DownlinesAdapter;
import com.webinfotech.grocerszone.threading.MainThreadImpl;

public class DownlinesActivity extends CommonBaseActivity implements DownlineActivityPresenter.View {

    @BindView(R.id.recycler_view_downlines)
    RecyclerView recyclerViewDownlines;
    ProgressDialog progressDialog;
    DownlineActivityPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_downlines);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Downlines");
        setUpProgressDialog();
        initialisePresenter();
        mPresenter.getDownlineList();
    }

    private void initialisePresenter() {
        mPresenter = new DownlineActivityPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void showProgressBar() {
        progressDialog.show();
    }

    @Override
    public void hideProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void loadData(DownlinesAdapter adapter) {
        recyclerViewDownlines.setAdapter(adapter);
        recyclerViewDownlines.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
