package com.webinfotech.grocerszone.presentation.ui.adapters;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.webinfotech.grocerszone.R;
import com.webinfotech.grocerszone.domain.models.Orders.OrderHistory;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Raj on 20-08-2019.
 */

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.ViewHolder> {

    Context mContext;
    OrderHistory[] orders;

    public OrdersAdapter(Context mContext, OrderHistory[] orders) {
        this.mContext = mContext;
        this.orders = orders;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_order_list, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {

        OrdersProductsListAdapter orderProductsAdapter = new OrdersProductsListAdapter(mContext, orders[i].products);
        viewHolder.recyclerViewOrderProducts.setAdapter(orderProductsAdapter);
        viewHolder.recyclerViewOrderProducts.setLayoutManager(new LinearLayoutManager(mContext));
        viewHolder.txtViewOrderId.setText(Integer.toString(orders[i].id));
        viewHolder.txtViewTotalAmount.setText("₹ " + orders[i].total);
        viewHolder.txtViewNetAmount.setText("₹ " + orders[i].payable_amount);
        viewHolder.txtViewCashback.setText("₹ " + orders[i].cashback);
        viewHolder.txtViewWalletAmount.setText("₹ " + orders[i].wallet_pay);
        viewHolder.txtViewTotalItems.setText(Integer.toString(orders[i].products.length));
        if(orders[i].delivery_status == 1){
            viewHolder.txtViewOrderStatus.setText("Pending");
            viewHolder.txtViewOrderStatus.setTextColor(mContext.getResources().getColor(R.color.md_blue_500));
            viewHolder.layoutBlue.setVisibility(View.VISIBLE);
            viewHolder.layoutGreen.setVisibility(View.GONE);
        }else{
            viewHolder.txtViewOrderStatus.setText("Delivered");
            viewHolder.txtViewOrderStatus.setTextColor(mContext.getResources().getColor(R.color.md_blue_500));
            viewHolder.layoutBlue.setVisibility(View.VISIBLE);
            viewHolder.layoutGreen.setVisibility(View.GONE);
        }
        viewHolder.txtViewDate.setText(orders[i].date);

    }

    @Override
    public int getItemCount() {
        return orders.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_order_id)
        TextView txtViewOrderId;
        @BindView(R.id.txt_view_net_amount)
        TextView txtViewNetAmount;
        @BindView(R.id.txt_view_total_amount)
        TextView txtViewTotalAmount;
        @BindView(R.id.txt_view_cashback)
        TextView txtViewCashback;
        @BindView(R.id.txt_view_order_status)
        TextView txtViewOrderStatus;
        @BindView(R.id.layout_blue)
        View layoutBlue;
        @BindView(R.id.layout_green)
        View layoutGreen;
        @BindView(R.id.layout_red)
        View layoutRed;
        @BindView(R.id.recycler_view_orders_products)
        RecyclerView recyclerViewOrderProducts;
        @BindView(R.id.txt_view_total_item)
        TextView txtViewTotalItems;
        @BindView(R.id.txt_view_wallet_amount)
        TextView txtViewWalletAmount;
        @BindView(R.id.txt_view_date)
        TextView txtViewDate;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }



}
