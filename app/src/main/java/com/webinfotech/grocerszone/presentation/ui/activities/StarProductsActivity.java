package com.webinfotech.grocerszone.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.AbsListView;

import com.webinfotech.grocerszone.R;
import com.webinfotech.grocerszone.domain.executors.impl.ThreadExecutor;
import com.webinfotech.grocerszone.presentation.presenters.StarProductsPresenter;
import com.webinfotech.grocerszone.presentation.presenters.impl.StarProductsPresenterImpl;
import com.webinfotech.grocerszone.presentation.ui.adapters.StarProductsAdapter;
import com.webinfotech.grocerszone.threading.MainThreadImpl;

public class StarProductsActivity extends AppCompatActivity implements StarProductsPresenter.View {

    @BindView(R.id.recycler_view_star_products)
    RecyclerView recyclerViewStarProducts;
    StarProductsPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int pageNo = 1;
    int totalPage = 1;
    int categoryId;
    LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_star_products);
        getSupportActionBar().setTitle("Star Products");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
        mPresenter.fetchStarProducts(pageNo, "refresh");
    }

    private void initialisePresenter() {
        mPresenter = new StarProductsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadData(StarProductsAdapter starProductsAdapter, int totalPage) {
        this.totalPage = totalPage;
        layoutManager = new GridLayoutManager(this,2);
        recyclerViewStarProducts.setLayoutManager(layoutManager);
        recyclerViewStarProducts.setAdapter(starProductsAdapter);
        recyclerViewStarProducts.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    if(pageNo < totalPage) {
                        isScrolling = false;
                        pageNo = pageNo + 1;
                        mPresenter.fetchStarProducts(pageNo, "");
                    }
                }
            }
        });
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
