package com.webinfotech.grocerszone.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.webinfotech.grocerszone.AndroidApplication;
import com.webinfotech.grocerszone.R;
import com.webinfotech.grocerszone.domain.executors.impl.ThreadExecutor;
import com.webinfotech.grocerszone.domain.models.User.UserInfo;
import com.webinfotech.grocerszone.presentation.presenters.CartDetailsActivityPresenter;
import com.webinfotech.grocerszone.presentation.presenters.impl.CartDetailsActivityPresenterImpl;
import com.webinfotech.grocerszone.presentation.ui.adapters.CartAdapter;
import com.webinfotech.grocerszone.presentation.ui.adapters.CartShippingAddressAdapter;
import com.webinfotech.grocerszone.presentation.ui.dialogs.DeliverySpeedDialog;
import com.webinfotech.grocerszone.threading.MainThreadImpl;

public class CartListActivity extends CommonBaseActivity implements CartDetailsActivityPresenter.View, DeliverySpeedDialog.Callback {

    @BindView(R.id.recycler_view_cart_list)
    RecyclerView recyclerViewCartList;
    CartDetailsActivityPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    @BindView(R.id.layout_summary)
    View layoutSummary;
    @BindView(R.id.txt_view_sub_total)
    TextView txtViewSubTotal;
    @BindView(R.id.txt_view_discount)
    TextView txtViewDiscount;
    @BindView(R.id.txt_view_grand_total)
    TextView txtViewGrandTotal;
    @BindView(R.id.btn_delivery_address)
    Button btnDeliveryAddress;
    BottomSheetDialog addressBottomSheetDialog;
    TextView txtViewAddAddress;
    ImageView imgViewAddAddress;
    RecyclerView recyclerViewShippingAddress;
    double grandTotal;
    @BindView(R.id.layout_delivery_charges)
    View layoutDeliveryCharges;
    @BindView(R.id.txt_view_order_warning)
    TextView txtViewWarning;
    DeliverySpeedDialog deliverySpeedDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_cart_list);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Cart");
        setAddressBottomSheetDialog();
        initialisePresenter();
        setUpProgressDialog();
        mPresenter.getCartList();
        mPresenter.fetchWalletAmount();
        initialiseDeliverySpeedDialog();
    }

    private void initialisePresenter() {
        mPresenter = new CartDetailsActivityPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void initialiseDeliverySpeedDialog() {
        deliverySpeedDialog = new DeliverySpeedDialog(this, this, this);
        deliverySpeedDialog.setUpDialogView();
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void setAddressBottomSheetDialog() {
        if (addressBottomSheetDialog == null) {
            View view = LayoutInflater.from(this).inflate(R.layout.layout_address_bottom_sheet, null);
            addressBottomSheetDialog = new BottomSheetDialog(this);
            txtViewAddAddress = (TextView) view.findViewById(R.id.txt_view_add_address);
            imgViewAddAddress = (ImageView) view.findViewById(R.id.img_view_add_address);
            recyclerViewShippingAddress = (RecyclerView) view.findViewById(R.id.recycler_view_address);
            txtViewAddAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), AddAddressActivity.class);
                    startActivity(intent);
                }
            });
            addressBottomSheetDialog.setContentView(view);
        }
    }

    @Override
    public void loadCartItemList(CartAdapter adapter, double subTotal, double discount, double grandTotal) {

        this.grandTotal = grandTotal;

        if (grandTotal < 200) {
            layoutDeliveryCharges.setVisibility(View.VISIBLE);
            this.grandTotal = this.grandTotal + 20;
        } else {
            layoutDeliveryCharges.setVisibility(View.GONE);
        }

        recyclerViewCartList.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewCartList.setAdapter(adapter);
        DividerItemDecoration itemDecor = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        recyclerViewCartList.addItemDecoration(itemDecor);

        layoutSummary.setVisibility(View.VISIBLE);
        txtViewSubTotal.setText("₹ " + subTotal);
        txtViewDiscount.setText("₹ " + discount);
        btnDeliveryAddress.setVisibility(View.VISIBLE);
        txtViewGrandTotal.setText("₹ " + this.grandTotal);

        txtViewWarning.setVisibility(View.VISIBLE);

    }

    @Override
    public void loadAddressAdapter(CartShippingAddressAdapter cartShippingAddressAdapter) {
        recyclerViewShippingAddress.setAdapter(cartShippingAddressAdapter);
        recyclerViewShippingAddress.setLayoutManager(new LinearLayoutManager(this));
        DividerItemDecoration itemDecor = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        recyclerViewShippingAddress.addItemDecoration(itemDecor);
    }

    @Override
    public void onDeliverButtonClicked() {
        deliverySpeedDialog.showDialog();
    }

    @Override
    public void goToOrderHistory() {
        Intent intent = new Intent(this, OrderHistoryActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void hideViews() {
        recyclerViewCartList.setVisibility(View.GONE);
        layoutSummary.setVisibility(View.GONE);
        btnDeliveryAddress.setVisibility(View.GONE);
        txtViewWarning.setVisibility(View.GONE);
    }

    @Override
    public void showLoadingProgress() {
        progressDialog.show();
    }

    @Override
    public void hideLoadingProgress() {
        progressDialog.dismiss();
    }

    @OnClick(R.id.btn_delivery_address) void onDeliveryAddressClicked() {
        addressBottomSheetDialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchShippingAddress();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override

    public void onProccedClicked() {
        deliverySpeedDialog.hideDialog();
        mPresenter.placeOrder();
    }
}
