package com.webinfotech.grocerszone.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.EditText;

import com.google.android.material.textfield.TextInputLayout;
import com.webinfotech.grocerszone.R;
import com.webinfotech.grocerszone.domain.executors.impl.ThreadExecutor;
import com.webinfotech.grocerszone.presentation.presenters.ForgetPasswordPresenter;
import com.webinfotech.grocerszone.presentation.presenters.impl.ForgetPasswordPresenterImpl;
import com.webinfotech.grocerszone.threading.MainThreadImpl;

public class ForgotPasswordActivity extends AppCompatActivity implements ForgetPasswordPresenter.View {

    @BindView(R.id.txt_input_email_layout)
    TextInputLayout txtInputEmailLayout;
    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    ProgressDialog progressDialog;
    ForgetPasswordPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Forgot Password");
        ButterKnife.bind(this);
        setUpProgressDialog();
        initialisePresenter();
    }

    private void initialisePresenter() {
        mPresenter = new ForgetPasswordPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {
        txtInputEmailLayout.setError("");
        if (editTextEmail.getText().toString().isEmpty()) {
            txtInputEmailLayout.setError("Please Enter Registered Email");
        } else {
            mPresenter.submitEmail(editTextEmail.getText().toString());
            showLoader();
        }
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void onSuccess() {
        editTextEmail.setText("");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
