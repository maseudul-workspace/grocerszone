package com.webinfotech.grocerszone.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.grocerszone.AndroidApplication;
import com.webinfotech.grocerszone.domain.executors.Executor;
import com.webinfotech.grocerszone.domain.executors.MainThread;
import com.webinfotech.grocerszone.domain.interactors.GetWalletHistoryInteractor;
import com.webinfotech.grocerszone.domain.interactors.impl.GetWalletHistoryInteractorImpl;
import com.webinfotech.grocerszone.domain.models.User.UserInfo;
import com.webinfotech.grocerszone.domain.models.Wallet.WalletDetails;
import com.webinfotech.grocerszone.domain.models.Wallet.WalletHistory;
import com.webinfotech.grocerszone.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.grocerszone.presentation.ui.adapters.WalletHistoryAdapter;
import com.webinfotech.grocerszone.repository.User.UserRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class WalletHistoryPresenterImpl extends AbstractPresenter implements com.webinfotech.grocerszone.presentation.presenters.WalletHistoryPresenter, GetWalletHistoryInteractor.Callback {

    Context mContext;
    WalletHistoryPresenterImpl.View mView;
    WalletHistoryAdapter adapter;
    WalletHistory[] newWalletHistories;
    GetWalletHistoryInteractorImpl getWalletHistoryInteractor;
    AndroidApplication androidApplication;

    public WalletHistoryPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchWalletHistory(int page, String type) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            getWalletHistoryInteractor = new GetWalletHistoryInteractorImpl(mExecutor, mMainThread, this, new UserRepositoryImpl(), userInfo.apiKey, userInfo.userId, page);
            getWalletHistoryInteractor.execute();
            if (type.equals("refresh")) {
                newWalletHistories = null;
                mView.showLoader();
            }
        } else {
            Toasty.error(mContext, "Your Session Expired!!! Please Login Again", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onGettingWalletHistorySuccess(WalletDetails walletDetails, int totalPage) {
        WalletHistory[] tempWalletHistory;
        tempWalletHistory = newWalletHistories;
        try {
            int len1 = tempWalletHistory.length;
            int len2 = walletDetails.walletHistories.length;
            newWalletHistories = new WalletHistory[len1 + len2];
            System.arraycopy(tempWalletHistory, 0, newWalletHistories, 0, len1);
            System.arraycopy(walletDetails.walletHistories.length, 0, newWalletHistories, len1, len2);
            adapter.updateDataSet(newWalletHistories);
            adapter.notifyDataSetChanged();
        } catch (NullPointerException e) {
            newWalletHistories = walletDetails.walletHistories;
            adapter = new WalletHistoryAdapter(mContext, newWalletHistories);
            mView.loadAdapter(adapter, walletDetails, totalPage);
        }
        mView.hideLoader();
    }

    @Override
    public void onGettingWalletHistoryFail(String errorMsg) {
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        mView.hideLoader();
    }


}
