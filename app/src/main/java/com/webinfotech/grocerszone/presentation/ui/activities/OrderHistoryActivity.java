package com.webinfotech.grocerszone.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;

import com.webinfotech.grocerszone.R;
import com.webinfotech.grocerszone.domain.executors.impl.ThreadExecutor;
import com.webinfotech.grocerszone.presentation.presenters.OrderHistoryPresenter;
import com.webinfotech.grocerszone.presentation.presenters.impl.OrderHistoryPresenterImpl;
import com.webinfotech.grocerszone.presentation.ui.adapters.OrdersAdapter;
import com.webinfotech.grocerszone.threading.MainThreadImpl;

public class OrderHistoryActivity extends CommonBaseActivity implements OrderHistoryPresenter.View {

    @BindView(R.id.recycler_view_orders)
    RecyclerView recyclerViewOrders;
    OrderHistoryPresenterImpl mPresenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_order_history);
        getSupportActionBar().setTitle("My Orders");
        ButterKnife.bind(this);
        setProgressDialog();
        initialisePresenter();
        mPresenter.getOrderHistory();
    }

    private void initialisePresenter() {
        mPresenter = new OrderHistoryPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadOrdersAdapter(OrdersAdapter adapter) {
        recyclerViewOrders.setAdapter(adapter);
        recyclerViewOrders.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void showProgressBar() {
        progressDialog.show();
    }

    @Override
    public void hideProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
