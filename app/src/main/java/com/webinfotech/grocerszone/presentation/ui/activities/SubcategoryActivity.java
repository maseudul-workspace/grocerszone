package com.webinfotech.grocerszone.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.webinfotech.grocerszone.R;
import com.webinfotech.grocerszone.domain.executors.impl.ThreadExecutor;
import com.webinfotech.grocerszone.presentation.presenters.SubcategoryPresenter;
import com.webinfotech.grocerszone.presentation.presenters.impl.SubcategoryPresenterImpl;
import com.webinfotech.grocerszone.presentation.ui.adapters.SubcategoryAdapter;
import com.webinfotech.grocerszone.threading.MainThreadImpl;

public class SubcategoryActivity extends AppCompatActivity implements SubcategoryPresenter.View {

    @BindView(R.id.recycler_view_sub_categories)
    RecyclerView recyclerViewSubcategories;
    SubcategoryPresenterImpl mPresenter;
    int catId;
    String categoryName;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subcategory);
        catId = getIntent().getIntExtra("catId", 0);
        categoryName = getIntent().getStringExtra("categoryName");
        getSupportActionBar().setTitle(categoryName);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        setUpProgressDialog();
        initialisePresenter();
        mPresenter.fetchSubcategories(catId);
        showLoader();
    }

    private void initialisePresenter() {
        mPresenter = new SubcategoryPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadSubcategoriesAdapter(SubcategoryAdapter subcategoryAdapter) {
        recyclerViewSubcategories.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewSubcategories.setAdapter(subcategoryAdapter);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void goToProductList(int subcategoryId, String subcategoryName) {
        Intent intent = new Intent(this, ProductListActivity.class);
        intent.putExtra("categoryId", subcategoryId);
        intent.putExtra("type", 2);
        intent.putExtra("categoryName", subcategoryName);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
