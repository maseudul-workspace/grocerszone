package com.webinfotech.grocerszone.presentation.ui.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.grocerszone.R;
import com.webinfotech.grocerszone.domain.models.Products.Product;
import com.webinfotech.grocerszone.util.GlideHelper;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductListVerticalAdapter extends RecyclerView.Adapter<ProductListVerticalAdapter.ViewHolder> {

    public interface Callback {
        void onProductClicked(int productId);
        void onAddToCartClicked(int productId);
    }

    Context mContext;
    Product[] products;
    Callback mCallback;

    public ProductListVerticalAdapter(Context mContext, Product[] products, Callback callback) {
        this.mContext = mContext;
        this.products = products;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_products_vertical, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GlideHelper.setImageViewCustomRoundedCorners(mContext, holder.imgViewProduct, mContext.getResources().getString(R.string.base_url) + "/uploads/product_image/thumb/" + products[position].image, 28);
        holder.txtViewProductName.setText(products[position].name);
        holder.txtViewProductPrice.setText("₹ " + products[position].price);
        holder.txtViewProductMrp.setText("₹ " + products[position].mrp);
        holder.txtViewProductMrp.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onProductClicked(products[position].id);
            }
        });
        holder.layoutCartAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onAddToCartClicked(products[position].id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return products.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.img_view_product)
        ImageView imgViewProduct;
        @BindView(R.id.txt_view_product_name)
        TextView txtViewProductName;
        @BindView(R.id.txt_view_product_price)
        TextView txtViewProductPrice;
        @BindView(R.id.layout_cart_add)
        View layoutCartAdd;
        @BindView(R.id.main_layout)
        View mainLayout;
        @BindView(R.id.txt_view_product_mrp)
        TextView txtViewProductMrp;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public void updateDataset(Product[] products) {
        this.products = products;
    }

}
