package com.webinfotech.grocerszone.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.webinfotech.grocerszone.R;
import com.webinfotech.grocerszone.domain.models.User.Downline;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class DownlinesAdapter extends RecyclerView.Adapter<DownlinesAdapter.ViewHolder> {

    Context mContext;
    Downline[] downlines;

    public DownlinesAdapter(Context mContext, Downline[] downlines) {
        this.mContext = mContext;
        this.downlines = downlines;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_downlines, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewDownlineName.setText(downlines[position].name);
        holder.txtViewDownlineMobile.setText(downlines[position].mobile);
    }

    @Override
    public int getItemCount() {
        return downlines.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_downline_name)
        TextView txtViewDownlineName;
        @BindView(R.id.txt_view_downline_mobile)
        TextView txtViewDownlineMobile;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

}
