package com.webinfotech.grocerszone.presentation.ui.activities;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.webinfotech.grocerszone.R;
import com.webinfotech.grocerszone.domain.executors.impl.ThreadExecutor;
import com.webinfotech.grocerszone.domain.models.ImageSliders.ImageSlider;
import com.webinfotech.grocerszone.domain.models.Testing.Brands;
import com.webinfotech.grocerszone.presentation.presenters.MainPresenter;
import com.webinfotech.grocerszone.presentation.presenters.impl.MainPresenterImpl;
import com.webinfotech.grocerszone.presentation.ui.adapters.BrandsAdapter;
import com.webinfotech.grocerszone.presentation.ui.adapters.HomeSlidingImagesAdapter;
import com.webinfotech.grocerszone.presentation.ui.adapters.MainCategoriesAdapter;
import com.webinfotech.grocerszone.presentation.ui.adapters.ProductHorizontalAdapter;
import com.webinfotech.grocerszone.threading.MainThreadImpl;
import com.webinfotech.grocerszone.util.GlideHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends BaseActivity implements MainPresenter.View {

    @BindView(R.id.viewpager) ViewPager viewPager;
    @BindView(R.id.dots_layout) LinearLayout dotsLinearLayout;
    @BindView(R.id.recycler_view_categories) RecyclerView recyclerViewCategories;
    @BindView(R.id.recycler_view_popular_products) RecyclerView recyclerViewPopularProducts;
    @BindView(R.id.recycler_view_star_products) RecyclerView recyclerViewStarProducts;
    @BindView(R.id.txt_view_cart_count)
    TextView txtViewCartCount;
    MainPresenterImpl mPresenter;
    private static int currentPage = 0;
    @BindView(R.id.txt_view_star_count)
    TextView txtViewStarCount;
    @BindView(R.id.recycler_view_trending_products)
    RecyclerView recyclerViewTrendingProducts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_main);
        Log.e("LogMsg", "Current Time: " + getTime() );
        initialisePresenter();
        mPresenter.getCategories();
        mPresenter.getMainData();
    }

    private void initialisePresenter() {
        mPresenter = new MainPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void prepareDotsIndicator(int sliderPosition, int imageCount){
        if(dotsLinearLayout.getChildCount() > 0){
            dotsLinearLayout.removeAllViews();
        }

        ImageView dots[] = new ImageView[imageCount];
        for(int i = 0; i < imageCount; i++){
            dots[i] = new ImageView(this);
            if(i == sliderPosition){
                GlideHelper.setImageViewWithDrawable(getApplicationContext(), dots[i], R.drawable.active_dot);
            }else{
                GlideHelper.setImageViewWithDrawable(getApplicationContext(), dots[i], R.drawable.inactive_dot);
            }

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(18, 18);
            layoutParams.setMargins(5, 0, 5, 0);
            dotsLinearLayout.addView(dots[i], layoutParams);

        }
    }

    @Override
    public void loadCategoriesAdapter(MainCategoriesAdapter adapter) {
        recyclerViewCategories.setAdapter(adapter);
        recyclerViewCategories.setLayoutManager(new GridLayoutManager(this, 2));
    }

    @Override
    public void goToSubcategoryActivity(int catId, String categoryName) {
        Intent intent = new Intent(this, SubcategoryActivity.class);
        intent.putExtra("catId", catId);
        intent.putExtra("categoryName", categoryName);
        startActivity(intent);
    }

    @Override
    public void goToProductList(int categoryId, String categoryName) {
        Intent intent = new Intent(this, ProductListActivity.class);
        intent.putExtra("categoryId", categoryId);
        intent.putExtra("type", 1);
        intent.putExtra("categoryName", categoryName);
        startActivity(intent);
    }

    @Override
    public void loadMainData(ProductHorizontalAdapter newProductsAdapter, ProductHorizontalAdapter popularProductsAdapter, ProductHorizontalAdapter trendingProductsAdapter, ImageSlider[] imageSliders) {
        recyclerViewPopularProducts.setAdapter(newProductsAdapter);
        recyclerViewPopularProducts.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        recyclerViewStarProducts.setAdapter(popularProductsAdapter);
        recyclerViewStarProducts.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        recyclerViewTrendingProducts.setAdapter(trendingProductsAdapter);
        recyclerViewTrendingProducts.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        HomeSlidingImagesAdapter homeSlidingImagesAdapter = new HomeSlidingImagesAdapter(this, imageSliders);
        viewPager.setAdapter(homeSlidingImagesAdapter);
        prepareDotsIndicator(0, imageSliders.length);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                prepareDotsIndicator(position, imageSliders.length);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if(currentPage == imageSliders.length)
                {
                    currentPage = 0;
                }
                viewPager.setCurrentItem(currentPage, true);
                currentPage++;
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 1000, 3000);
    }

    @Override
    public void showLoader() {

    }

    @Override
    public void hideLoader() {

    }

    @Override
    public void goToProductDetails(int productId) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId", productId);
        startActivity(intent);
    }

    @Override
    public void loadCartCount(int count) {
        if (count == 0) {
            txtViewCartCount.setVisibility(View.GONE);
        } else {
            txtViewCartCount.setVisibility(View.VISIBLE);
            txtViewCartCount.setText(Integer.toString(count));
        }
    }

    @Override
    public void loadStarProductsCount(int count) {
        if (count == 0) {
            txtViewStarCount.setVisibility(View.INVISIBLE);
        } else {
            txtViewStarCount.setVisibility(View.VISIBLE);
            txtViewStarCount.setText(Integer.toString(count));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchCartCount();
        mPresenter.fetchStarProductsCount();
    }

    public static String getTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "hh:mm:ss a", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

}
