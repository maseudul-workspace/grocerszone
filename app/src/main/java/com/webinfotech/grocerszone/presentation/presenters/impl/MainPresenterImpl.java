package com.webinfotech.grocerszone.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.grocerszone.AndroidApplication;
import com.webinfotech.grocerszone.domain.executors.Executor;
import com.webinfotech.grocerszone.domain.executors.MainThread;
import com.webinfotech.grocerszone.domain.interactors.FetchStarProductsCountInteractor;
import com.webinfotech.grocerszone.domain.interactors.GetCartDetailsInteractor;
import com.webinfotech.grocerszone.domain.interactors.GetCategoryListInteractor;
import com.webinfotech.grocerszone.domain.interactors.GetMainDataInteractor;
import com.webinfotech.grocerszone.domain.interactors.impl.FetchStarProductsCountInteractorImpl;
import com.webinfotech.grocerszone.domain.interactors.impl.GetCartDetailsInteractorImpl;
import com.webinfotech.grocerszone.domain.interactors.impl.GetCategoryListInteractorImpl;
import com.webinfotech.grocerszone.domain.interactors.impl.GetMainDataInteractorImpl;
import com.webinfotech.grocerszone.domain.models.Cart.Cart;
import com.webinfotech.grocerszone.domain.models.Category.Category;
import com.webinfotech.grocerszone.domain.models.MainData;
import com.webinfotech.grocerszone.domain.models.User.UserInfo;
import com.webinfotech.grocerszone.presentation.presenters.MainPresenter;
import com.webinfotech.grocerszone.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.grocerszone.presentation.ui.adapters.MainCategoriesAdapter;
import com.webinfotech.grocerszone.presentation.ui.adapters.ProductHorizontalAdapter;
import com.webinfotech.grocerszone.repository.Category.CategoryRepositoryImpl;
import com.webinfotech.grocerszone.repository.ProductRepository.impl.CartRepositoryImpl;
import com.webinfotech.grocerszone.repository.ProductRepository.impl.GetProductsRepositoryImpl;
import com.webinfotech.grocerszone.repository.User.UserRepositoryImpl;

public class MainPresenterImpl extends AbstractPresenter implements MainPresenter,
                                                                    GetCategoryListInteractor.Callback,
                                                                    MainCategoriesAdapter.Callback,
                                                                    GetMainDataInteractor.Callback,
                                                                    ProductHorizontalAdapter.Callback,
                                                                    GetCartDetailsInteractor.Callback,
                                                                    FetchStarProductsCountInteractor.Callback

{

    Context mContext;
    MainPresenter.View mView;
    GetCategoryListInteractorImpl getCategoryListInteractor;
    GetMainDataInteractorImpl getMainDataInteractor;
    GetCartDetailsInteractorImpl getCartDetailsInteractor;
    FetchStarProductsCountInteractorImpl fetchStarProductsCountInteractor;

    public MainPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void getCategories() {
        getCategoryListInteractor = new GetCategoryListInteractorImpl(mExecutor, mMainThread, new CategoryRepositoryImpl(), this);
        getCategoryListInteractor.execute();
    }

    @Override
    public void getMainData() {
        getMainDataInteractor = new GetMainDataInteractorImpl(mExecutor, mMainThread, new GetProductsRepositoryImpl(), this);
        getMainDataInteractor.execute();
    }

    @Override
    public void fetchCartCount() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            getCartDetailsInteractor = new GetCartDetailsInteractorImpl(mExecutor,
                    mMainThread,
                    new CartRepositoryImpl(),
                    this,
                    userInfo.userId,
                    userInfo.apiKey);
            getCartDetailsInteractor.execute();
        }
    }

    @Override
    public void fetchStarProductsCount() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            fetchStarProductsCountInteractor = new FetchStarProductsCountInteractorImpl(mExecutor, mMainThread, new UserRepositoryImpl(), this, userInfo.userId, userInfo.apiKey);
            fetchStarProductsCountInteractor.execute();
        }
    }

    @Override
    public void onGettingCategoryListSuccess(Category[] categories) {
        MainCategoriesAdapter mainCategoriesAdapter = new MainCategoriesAdapter(mContext, categories, this);
        mView.loadCategoriesAdapter(mainCategoriesAdapter);
    }

    @Override
    public void onGettingCategoryFail(String errorMsg) {

    }

    @Override
    public void onCategoryClicked(int catId, String categoryName, boolean subcategoryStatus) {
        if (subcategoryStatus) {
            mView.goToSubcategoryActivity(catId, categoryName);
        } else {
            mView.goToProductList(catId, categoryName);
        }
    }

    @Override
    public void onGettingMainDataSuccess(MainData mainData) {
        ProductHorizontalAdapter newProductsAdapter = new ProductHorizontalAdapter(mContext, mainData.newArrivals, this);
        ProductHorizontalAdapter popularProductsAdapter = new ProductHorizontalAdapter(mContext, mainData.popularProducts, this);
        ProductHorizontalAdapter trendingProductsAdapter = new ProductHorizontalAdapter(mContext, mainData.trendingProducts, this);
        mView.loadMainData(newProductsAdapter, popularProductsAdapter, trendingProductsAdapter, mainData.imageSliders);
    }

    @Override
    public void onGettingMainDataFail(String errorMsg) {

    }

    @Override
    public void onProductClicked(int productId) {
        mView.goToProductDetails(productId);
    }

    @Override
    public void onGetCartDetailsSuccess(Cart[] carts) {
        mView.loadCartCount(carts.length);
    }

    @Override
    public void onGetCartDetailsFail(String errorMsg) {
        mView.loadCartCount(0);
    }

    @Override
    public void onGettingStarProductsCountSuccess(int starProductsCount) {
        mView.loadStarProductsCount(starProductsCount);
    }

    @Override
    public void onGettingStarProductsCountFail(String errorMsg) {
        mView.loadStarProductsCount(0);
    }

}
