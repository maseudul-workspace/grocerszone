package com.webinfotech.grocerszone.presentation.presenters;


import com.webinfotech.grocerszone.presentation.presenters.base.BasePresenter;
import com.webinfotech.grocerszone.presentation.ui.adapters.ProductListVerticalAdapter;

/**
 * Created by Raj on 27-02-2019.
 */

public interface SearchActivityPresenter extends BasePresenter {

    void getProductList(String search_key, int page_no, String type);

    interface View{
        void loadRecyclerViewAdapter(ProductListVerticalAdapter adapter, int totalPage);
        void showProgressBar();
        void hideProgressBar();
        void showLoginSnackBar();
        void showCartSnackbar();
        void goToProductDetails(int productId);
    }
}
