package com.webinfotech.grocerszone.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.webinfotech.grocerszone.R;
import com.webinfotech.grocerszone.domain.executors.impl.ThreadExecutor;
import com.webinfotech.grocerszone.domain.models.Products.ProductDetailsData;
import com.webinfotech.grocerszone.presentation.presenters.ProductDetailsPresenter;
import com.webinfotech.grocerszone.presentation.presenters.impl.ProductDetailsPresenterImpl;
import com.webinfotech.grocerszone.presentation.ui.adapters.ProductHorizontalAdapter;
import com.webinfotech.grocerszone.threading.MainThreadImpl;
import com.webinfotech.grocerszone.util.GlideHelper;

public class ProductDetailsActivity extends AppCompatActivity implements ProductDetailsPresenter.View {

    @BindView(R.id.dots_layout)
    LinearLayout dotsLinearLayout;
    @BindView(R.id.recycler_view_related_products)
    RecyclerView recyclerViewRelatedProducts;
    @BindView(R.id.img_view_product)
    ImageView imgViewProduct;
    @BindView(R.id.txt_view_product_name)
    TextView txtViewProductName;
    @BindView(R.id.txt_view_product_price)
    TextView txtViewProductPrice;
    @BindView(R.id.txt_view_product_mrp)
    TextView txtViewProductMrp;
    @BindView(R.id.txt_view_qty)
    TextView txtViewQty;
    @BindView(R.id.txt_view_product_desc)
    TextView txtViewProductDesc;
    @BindView(R.id.main_layout)
    View mainLayout;
    ProductDetailsPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    int productId;
    int quantity = 1;
    int stock;
    @BindView(R.id.txt_view_qty_status)
    TextView txtViewQtyStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        productId = getIntent().getIntExtra("productId", 0);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initialisePresenter();
        setUpProgressDialog();
        mPresenter.fetchProductDetails(productId);
        showLoader();
    }

    private void initialisePresenter() {
        mPresenter = new ProductDetailsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }


    @Override
    public void loadProductDetails(ProductDetailsData product, ProductHorizontalAdapter adapter) {
        mainLayout.setVisibility(View.VISIBLE);
        txtViewProductName.setText(product.name);
        txtViewProductDesc.setText(product.description);
        GlideHelper.setImageView(this, imgViewProduct, getResources().getString(R.string.base_url) + "/uploads/product_image/" + product.image);
        txtViewProductPrice.setText("₹ " + product.price);
        txtViewProductMrp.setText("₹ " + product.mrp);
        txtViewProductMrp.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        getSupportActionBar().setTitle(product.name);
        stock = product.stock;
        if (adapter != null) {
            recyclerViewRelatedProducts.setAdapter(adapter);
            recyclerViewRelatedProducts.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        }
        if (stock <= 5) {
            txtViewQtyStatus.setText("Only " + stock + " items left");
        }
    }

    @Override
    public void showCartSnackbar() {
        Snackbar snackbar = Snackbar.make(mainLayout,"Added To Cart",Snackbar.LENGTH_LONG);
        snackbar.setAction("Go To Cart", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cartIntent = new Intent(getApplicationContext(), CartListActivity.class);
                startActivity(cartIntent);
            }
        });
        snackbar.show();
    }

    @Override
    public void showLoginSnackbar() {
        Snackbar snackbar = Snackbar.make(mainLayout,"You must be logged in",Snackbar.LENGTH_LONG);
        snackbar.setAction("Log In", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cartIntent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(cartIntent);
            }
        });
        snackbar.show();
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void goToProductDetails(int productId) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId", productId);
        startActivity(intent);
    }

    @OnClick(R.id.layout_minus_qty) void onMinusQtyClicked() {
        if (quantity > 1) {
            quantity = quantity - 1;
            txtViewQty.setText("Quantity(" + quantity + ")");
        }
    }

    @OnClick(R.id.layout_plus_qty) void onPlusQtyClicked() {
        if (quantity < stock) {
            quantity = quantity + 1;
            txtViewQty.setText("Quantity(" + quantity + ")");
        }
    }

    @OnClick(R.id.btn_add_to_cart) void onAddToCartClicked() {
        mPresenter.addToCart(productId, quantity);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
