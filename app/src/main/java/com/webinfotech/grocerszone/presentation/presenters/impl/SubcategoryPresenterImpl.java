package com.webinfotech.grocerszone.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.grocerszone.domain.executors.Executor;
import com.webinfotech.grocerszone.domain.executors.MainThread;
import com.webinfotech.grocerszone.domain.interactors.GetSubcategoryListInteractor;
import com.webinfotech.grocerszone.domain.interactors.impl.GetSubcategoryListInteractorImpl;
import com.webinfotech.grocerszone.domain.models.Category.SubCategory;
import com.webinfotech.grocerszone.presentation.presenters.SubcategoryPresenter;
import com.webinfotech.grocerszone.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.grocerszone.presentation.ui.adapters.SubcategoryAdapter;
import com.webinfotech.grocerszone.repository.Category.CategoryRepositoryImpl;

public class SubcategoryPresenterImpl extends AbstractPresenter implements SubcategoryPresenter, GetSubcategoryListInteractor.Callback, SubcategoryAdapter.Callback {

    Context mContext;
    SubcategoryPresenter.View mView;
    GetSubcategoryListInteractorImpl getSubcategoryListInteractor;

    public SubcategoryPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchSubcategories(int catId) {
        getSubcategoryListInteractor = new GetSubcategoryListInteractorImpl(mExecutor, mMainThread, new CategoryRepositoryImpl(), this, catId);
        getSubcategoryListInteractor.execute();
    }

    @Override
    public void onGettingSubcategoriesSuccess(SubCategory[] subcategories) {
        SubcategoryAdapter subcategoryAdapter = new SubcategoryAdapter(mContext, subcategories, this);
        mView.loadSubcategoriesAdapter(subcategoryAdapter);
        mView.hideLoader();
    }

    @Override
    public void onGettingSubcategoriesFail(String errorMsg) {
        mView.hideLoader();
    }

    @Override
    public void onSubcategoryClicked(int subcategoryId, String subcategoryName) {
        mView.goToProductList(subcategoryId, subcategoryName);
    }
}
