package com.webinfotech.grocerszone.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.AbsListView;
import android.widget.TextView;

import com.webinfotech.grocerszone.R;
import com.webinfotech.grocerszone.domain.executors.impl.ThreadExecutor;
import com.webinfotech.grocerszone.domain.models.Wallet.WalletDetails;
import com.webinfotech.grocerszone.presentation.presenters.WalletHistoryPresenter;
import com.webinfotech.grocerszone.presentation.presenters.impl.WalletHistoryPresenterImpl;
import com.webinfotech.grocerszone.presentation.ui.adapters.WalletHistoryAdapter;
import com.webinfotech.grocerszone.threading.MainThreadImpl;

public class WalletHistoryActivity extends CommonBaseActivity implements WalletHistoryPresenter.View {

    @BindView(R.id.recycler_view_wallet_history)
    RecyclerView recyclerViewWalletHistory;
    @BindView(R.id.txt_view_active_amount)
    TextView txtViewActiveAmount;
    @BindView(R.id.txt_view_inactive_amount)
    TextView txtViewInactiveAmount;
    @BindView(R.id.txt_view_total_amount)
    TextView txtViewWalletAmount;
    WalletHistoryPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int pageNo = 1;
    int totalPage = 1;
    LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_wallet_history);
        getSupportActionBar().setTitle("Wallet");
        ButterKnife.bind(this);
        setProgressDialog();
        initialisePresenter();
        showLoader();
        mPresenter.fetchWalletHistory(pageNo, "refresh");
    }

    private void initialisePresenter() {
        mPresenter = new WalletHistoryPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }


    @Override
    public void loadAdapter(WalletHistoryAdapter adapter, WalletDetails walletDetails, int totalPage) {
        this.totalPage = totalPage;
        layoutManager = new LinearLayoutManager(this);
        recyclerViewWalletHistory.setLayoutManager(layoutManager);
        recyclerViewWalletHistory.setAdapter(adapter);
        recyclerViewWalletHistory.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    if(pageNo < totalPage) {
                        isScrolling = false;
                        pageNo = pageNo + 1;
                        mPresenter.fetchWalletHistory(pageNo, "");
                    }
                }
            }
        });
        txtViewActiveAmount.setText("₹ " + walletDetails.activeAmount);
        txtViewInactiveAmount.setText("₹ " + walletDetails.inactiveAmount);
        txtViewWalletAmount.setText("₹ " + walletDetails.totalAmount);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
