package com.webinfotech.grocerszone.presentation.presenters;

import com.webinfotech.grocerszone.domain.models.User.ShippingAddress;

public interface EditAddressPresenter {
    void fetchShippingAddress(int position);
    void updateShippingAddress(
            int addressId,
            String mobile,
            String email,
            String state,
            String city,
            String address,
            String pin
    );
    interface View {
        void loadAddress(ShippingAddress shippingAddress);
        void onAddressEditSuccess();
        void showLoader();
        void hideLoader();
    }
}
