package com.webinfotech.grocerszone.presentation.presenters;

import com.webinfotech.grocerszone.domain.models.ImageSliders.ImageSlider;
import com.webinfotech.grocerszone.presentation.ui.adapters.MainCategoriesAdapter;
import com.webinfotech.grocerszone.presentation.ui.adapters.ProductHorizontalAdapter;

public interface MainPresenter {
    void getCategories();
    void getMainData();
    void fetchCartCount();
    void fetchStarProductsCount();
    interface View {
        void loadCategoriesAdapter(MainCategoriesAdapter adapter);
        void goToSubcategoryActivity(int catId, String categoryName);
        void goToProductList(int categoryId, String categoryName);
        void loadMainData(ProductHorizontalAdapter newProductsAdapter, ProductHorizontalAdapter popularProductsAdapter, ProductHorizontalAdapter trendingProductsAdapter, ImageSlider[] imageSliders);
        void showLoader();
        void hideLoader();
        void goToProductDetails(int productId);
        void loadCartCount(int count);
        void loadStarProductsCount(int count);
    }
}
