package com.webinfotech.grocerszone.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;

import com.google.android.material.snackbar.Snackbar;
import com.webinfotech.grocerszone.R;
import com.webinfotech.grocerszone.domain.executors.impl.ThreadExecutor;
import com.webinfotech.grocerszone.presentation.presenters.SearchActivityPresenter;
import com.webinfotech.grocerszone.presentation.presenters.impl.SearchActivityPresenterImpl;
import com.webinfotech.grocerszone.presentation.ui.adapters.ProductListVerticalAdapter;
import com.webinfotech.grocerszone.threading.MainThreadImpl;

public class SearchActivity extends AppCompatActivity implements SearchActivityPresenter.View {

    SearchView searchView;
    @BindView(R.id.recycler_view_product_list_vertical)
    RecyclerView recyclerViewProductListVertical;
    @BindView(R.id.main_layout)
    View mainLayout;
    SearchActivityPresenterImpl mPresenter;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int pageNo = 1;
    int totalPage = 1;
    String searchKey;
    LinearLayoutManager layoutManager;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Search Items");
        initialisePresenter();
        setUpProgressDialog();
    }

    private void initialisePresenter() {
        mPresenter = new SearchActivityPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public boolean onCreateOptionsMenu( Menu menu) {
        getMenuInflater().inflate( R.menu.search_menu, menu);

        MenuItem myActionMenuItem = menu.findItem( R.id.action_search);
        searchView = (SearchView) myActionMenuItem.getActionView();
        myActionMenuItem.expandActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(!query.isEmpty()){
                    pageNo = 1;
                    totalPage = 1;
                    searchKey = query;
                    mPresenter.getProductList(searchKey, pageNo, "refresh");
                }
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                // UserFeedback.show( "SearchOnQueryTextChanged: " + s);
                if(!s.isEmpty()){
                    pageNo = 1;
                    totalPage = 1;
                    searchKey = s;
                    mPresenter.getProductList(searchKey, pageNo, "refresh");
                }
                return false;
            }
        });
        return true;
    }

    @Override
    public void loadRecyclerViewAdapter(ProductListVerticalAdapter adapter, int totalPage) {
        this.totalPage = totalPage;
        layoutManager = new GridLayoutManager(this,2);
        recyclerViewProductListVertical.setLayoutManager(layoutManager);
        recyclerViewProductListVertical.setAdapter(adapter);
        recyclerViewProductListVertical.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    if(pageNo < totalPage) {
                        isScrolling = false;
                        pageNo = pageNo + 1;
                        mPresenter.getProductList(searchKey, pageNo, "");
                    }
                }
            }
        });
    }

    @Override
    public void showProgressBar() {
        progressDialog.show();
    }

    @Override
    public void hideProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void showLoginSnackBar() {
        Snackbar snackbar = Snackbar.make(mainLayout,"You must be logged in",Snackbar.LENGTH_LONG);
        snackbar.setAction("Log In", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cartIntent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(cartIntent);
            }
        });
        snackbar.show();
    }

    @Override
    public void showCartSnackbar() {
        Snackbar snackbar = Snackbar.make(mainLayout,"Added To Cart",Snackbar.LENGTH_LONG);
        snackbar.setAction("Go To Cart", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cartIntent = new Intent(getApplicationContext(), CartListActivity.class);
                startActivity(cartIntent);
            }
        });
        snackbar.show();
    }

    @Override
    public void goToProductDetails(int productId) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId", productId);
        startActivity(intent);
    }
}
