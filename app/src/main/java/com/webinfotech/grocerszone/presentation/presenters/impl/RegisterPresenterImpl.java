package com.webinfotech.grocerszone.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.grocerszone.domain.executors.Executor;
import com.webinfotech.grocerszone.domain.executors.MainThread;
import com.webinfotech.grocerszone.domain.interactors.CreateUserInteractor;
import com.webinfotech.grocerszone.domain.interactors.impl.CreateUserInteractorImpl;
import com.webinfotech.grocerszone.presentation.presenters.RegisterPresenter;
import com.webinfotech.grocerszone.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.grocerszone.repository.User.UserRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class RegisterPresenterImpl extends AbstractPresenter implements RegisterPresenter,CreateUserInteractor.Callback {

    Context mContext;
    RegisterPresenter.View mView;
    CreateUserInteractorImpl createUserInteractor;

    public RegisterPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void createUser(String name, String mobile, String email, String password, String state, String city, String address, String pin) {
        createUserInteractor = new CreateUserInteractorImpl(mExecutor, mMainThread, new UserRepositoryImpl(), this, name, mobile, email, password, state, city, address, pin);
        createUserInteractor.execute();
    }

    @Override
    public void onUserCreateSuccess() {
        mView.onSignUpSuccess();
        mView.hideLoader();
        Toasty.success(mContext, "User Created Successfully", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUserCreateFail(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

}
