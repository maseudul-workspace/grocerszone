package com.webinfotech.grocerszone.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.grocerszone.AndroidApplication;
import com.webinfotech.grocerszone.domain.executors.Executor;
import com.webinfotech.grocerszone.domain.executors.MainThread;
import com.webinfotech.grocerszone.domain.interactors.FetchDownlineInteractor;
import com.webinfotech.grocerszone.domain.interactors.impl.FetchDownlineListInteractorImpl;
import com.webinfotech.grocerszone.domain.models.User.Downline;
import com.webinfotech.grocerszone.domain.models.User.UserInfo;
import com.webinfotech.grocerszone.presentation.presenters.DownlineActivityPresenter;
import com.webinfotech.grocerszone.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.grocerszone.presentation.ui.adapters.DownlinesAdapter;
import com.webinfotech.grocerszone.repository.User.UserRepositoryImpl;

import es.dmoral.toasty.Toasty;

/**
 * Created by Raj on 27-02-2019.
 */

public class DownlineActivityPresenterImpl extends AbstractPresenter implements DownlineActivityPresenter,
                                                                                FetchDownlineInteractor.Callback{

    Context mContext;
    FetchDownlineListInteractorImpl mInteractor;
    DownlineActivityPresenter.View mView;
    AndroidApplication androidApplication;
    UserInfo userInfo;
    DownlinesAdapter adapter;

    public DownlineActivityPresenterImpl(Executor executor,
                                         MainThread mainThread,
                                         Context context,
                                         DownlineActivityPresenter.View view
                                         ) {
        super(executor, mainThread);
        this.mContext = context;
        this.mView = view;
    }

    @Override
    public void getDownlineList() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            mInteractor = new FetchDownlineListInteractorImpl(mExecutor, mMainThread, this,
                    new UserRepositoryImpl(), userInfo.apiKey, userInfo.userId);
            mInteractor.execute();
            mView.showProgressBar();
        } else {
            Toasty.warning(mContext, "Your Session Is Expired!! Please Login Again").show();
        }
    }

    @Override
    public void onGettingDownlineListSuccess(Downline[] downlines) {
        adapter = new DownlinesAdapter(mContext, downlines);
        mView.loadData(adapter);
        mView.hideProgressBar();
    }

    @Override
    public void onGettingDownlineListFail(String errorMsg) {
        mView.hideProgressBar();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
