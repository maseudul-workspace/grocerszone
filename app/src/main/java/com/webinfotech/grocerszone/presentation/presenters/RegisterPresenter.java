package com.webinfotech.grocerszone.presentation.presenters;

public interface RegisterPresenter {
    void createUser(
            String name,
            String mobile,
            String email,
            String password,
            String state,
            String city,
            String address,
            String pin
    );
    interface View {
        void onSignUpSuccess();
        void showLoader();
        void hideLoader();
    }
}
