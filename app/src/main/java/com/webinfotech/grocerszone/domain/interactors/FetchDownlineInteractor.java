package com.webinfotech.grocerszone.domain.interactors;

import com.webinfotech.grocerszone.domain.models.User.Downline;

/**
 * Created by Raj on 27-02-2019.
 */

public interface FetchDownlineInteractor {
    interface Callback{
        void onGettingDownlineListSuccess(Downline[] downlines);
        void onGettingDownlineListFail(String errorMsg);
    }
}
