package com.webinfotech.grocerszone.domain.interactors;

import com.webinfotech.grocerszone.domain.models.User.ShippingAddress;

public interface FetchShippingAddressInteractor {
    interface Callback {
        void onAddressFetchSuccess(ShippingAddress[] shippingAddresses);
        void onAddressFetchFail(String errorMsg);
    }
}
