package com.webinfotech.grocerszone.domain.interactors.impl;

import com.webinfotech.grocerszone.domain.executors.Executor;
import com.webinfotech.grocerszone.domain.executors.MainThread;
import com.webinfotech.grocerszone.domain.interactors.ForgetPasswordInteractor;
import com.webinfotech.grocerszone.domain.interactors.base.AbstractInteractor;
import com.webinfotech.grocerszone.domain.models.User.ForgotPasswordResponse;
import com.webinfotech.grocerszone.repository.User.UserRepositoryImpl;

/**
 * Created by Raj on 25-06-2019.
 */

public class ForgetPasswordInteractorImpl extends AbstractInteractor implements ForgetPasswordInteractor {

    Callback mCallback;
    UserRepositoryImpl mRepository;
    String email;

    public ForgetPasswordInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, UserRepositoryImpl mRepository, String email) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.email = email;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onForgetPasswordFail(errorMsg);
            }
        });
    }

    private void postMessage(final String successMsg){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onForgetPasswordSuccess(successMsg);
            }
        });
    }

    @Override
    public void run() {
        final ForgotPasswordResponse forgotPasswordResponse = mRepository.forgotPassword(email);
        if(forgotPasswordResponse == null){
            notifyError("Something went wrong");
        }else if(!forgotPasswordResponse.status){
            notifyError(forgotPasswordResponse.message);
        }else{
            postMessage(forgotPasswordResponse.message);
        }
    }
}
