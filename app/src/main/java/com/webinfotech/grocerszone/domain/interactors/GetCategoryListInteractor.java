package com.webinfotech.grocerszone.domain.interactors;

import com.webinfotech.grocerszone.domain.models.Category.Category;

public interface GetCategoryListInteractor {
    interface Callback {
        void onGettingCategoryListSuccess(Category[] categories);
        void onGettingCategoryFail(String errorMsg);
    }
}
