package com.webinfotech.grocerszone.domain.models.Wallet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 22-02-2019.
 */

public class WalletHistory {

    @SerializedName("transaction_type")
    @Expose
    public int transaction_type;

    @SerializedName("transaction_amount")
    @Expose
    public double transaction_amount;

    @SerializedName("total_amount")
    @Expose
    public double totalAmount;

    @SerializedName("comment")
    @Expose
    public String comment;

    @SerializedName("date")
    @Expose
    public String date;

    @SerializedName("time")
    @Expose
    public String time;

}
