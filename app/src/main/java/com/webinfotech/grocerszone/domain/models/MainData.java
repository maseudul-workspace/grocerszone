package com.webinfotech.grocerszone.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webinfotech.grocerszone.domain.models.ImageSliders.ImageSlider;
import com.webinfotech.grocerszone.domain.models.Products.Product;

public class MainData {

    @SerializedName("sliders")
    @Expose
    public ImageSlider[] imageSliders;

    @SerializedName("new_arrivals")
    @Expose
    public Product[] newArrivals;

    @SerializedName("polular_products")
    @Expose
    public Product[] popularProducts;

    @SerializedName("trending_products")
    @Expose
    public Product[] trendingProducts;

}
