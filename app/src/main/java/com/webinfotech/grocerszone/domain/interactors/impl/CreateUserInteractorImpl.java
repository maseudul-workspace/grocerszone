package com.webinfotech.grocerszone.domain.interactors.impl;

import com.webinfotech.grocerszone.domain.executors.Executor;
import com.webinfotech.grocerszone.domain.executors.MainThread;
import com.webinfotech.grocerszone.domain.interactors.CreateUserInteractor;
import com.webinfotech.grocerszone.domain.interactors.base.AbstractInteractor;
import com.webinfotech.grocerszone.domain.models.CommonResponse;
import com.webinfotech.grocerszone.repository.User.UserRepositoryImpl;

public class CreateUserInteractorImpl extends AbstractInteractor implements CreateUserInteractor {

    UserRepositoryImpl mRepository;
    Callback mCallback;
    String name;
    String mobile;
    String email;
    String password;
    String state;
    String city;
    String address;
    String pin;

    public CreateUserInteractorImpl(Executor threadExecutor, MainThread mainThread, UserRepositoryImpl mRepository, Callback mCallback, String name, String mobile, String email, String password, String state, String city, String address, String pin) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.name = name;
        this.mobile = mobile;
        this.email = email;
        this.password = password;
        this.state = state;
        this.city = city;
        this.address = address;
        this.pin = pin;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUserCreateFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUserCreateSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.signUp(name, mobile, email, password, state, city, address, pin);
        if (commonResponse == null) {
            notifyError("Something Went Wrong");
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message);
        } else {
            postMessage();
        }
    }
}
