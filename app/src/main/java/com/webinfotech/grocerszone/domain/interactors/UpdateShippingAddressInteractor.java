package com.webinfotech.grocerszone.domain.interactors;

public interface UpdateShippingAddressInteractor {
    interface Callback {
        void onEditAddressSuccess();
        void onEditAddressFail(String errorMsg);
    }
}
