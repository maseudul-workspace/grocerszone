package com.webinfotech.grocerszone.domain.models.User;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 13-02-2019.
 */

public class UserDetails {
    @SerializedName("user_id")
    @Expose
    public int userId;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("mobile")
    @Expose
    public long mobile;

    @SerializedName("parmanent_address")
    @Expose
    public PermanentAddress permanentAddress;

}
