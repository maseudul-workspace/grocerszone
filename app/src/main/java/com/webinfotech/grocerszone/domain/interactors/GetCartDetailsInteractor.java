package com.webinfotech.grocerszone.domain.interactors;


import com.webinfotech.grocerszone.domain.interactors.base.Interactor;
import com.webinfotech.grocerszone.domain.models.Cart.Cart;

/**
 * Created by Raj on 10-01-2019.
 */

public interface GetCartDetailsInteractor extends Interactor {
    interface Callback{
        void onGetCartDetailsSuccess(Cart[] carts);
        void onGetCartDetailsFail(String errorMsg);
    }
}
