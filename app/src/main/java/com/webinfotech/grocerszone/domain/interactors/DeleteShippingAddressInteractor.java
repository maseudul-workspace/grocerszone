package com.webinfotech.grocerszone.domain.interactors;

public interface DeleteShippingAddressInteractor {
    interface Callback {
        void onAddressDeleteSuccess();
        void onAddressDeleteFail(String errorMsg);
    }
}
