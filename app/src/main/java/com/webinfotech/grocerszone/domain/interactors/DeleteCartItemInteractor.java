package com.webinfotech.grocerszone.domain.interactors;

import com.webinfotech.grocerszone.domain.interactors.base.Interactor;

/**
 * Created by Raj on 10-01-2019.
 */

public interface DeleteCartItemInteractor extends Interactor {
    interface Callback{
        void onCartItemDeletedSuccess(String successMsg);
        void onCartItemDeleteFail(String errorMsg);
    }
}
