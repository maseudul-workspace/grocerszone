package com.webinfotech.grocerszone.domain.interactors;


import com.webinfotech.grocerszone.domain.interactors.base.Interactor;

/**
 * Created by Raj on 09-01-2019.
 */

public interface UpdateCartInteractor extends Interactor {
    interface Callback {
        void onUpdateCartSuccess(String successMsg);
        void onUpdateCartFail(String errorMsg);
    }
}
