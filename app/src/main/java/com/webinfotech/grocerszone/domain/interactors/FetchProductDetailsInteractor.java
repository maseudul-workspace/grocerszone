package com.webinfotech.grocerszone.domain.interactors;

import com.webinfotech.grocerszone.domain.models.Products.ProductDetailsData;

public interface FetchProductDetailsInteractor {
    interface Callback {
        void onGettingProductDetailsSuccess(ProductDetailsData product);
        void onGettingProductDetailsFail(String errorMsg);
    }
}
