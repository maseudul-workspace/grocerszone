package com.webinfotech.grocerszone.domain.interactors.impl;

import com.webinfotech.grocerszone.domain.executors.Executor;
import com.webinfotech.grocerszone.domain.executors.MainThread;
import com.webinfotech.grocerszone.domain.interactors.CheckLoginInteractor;
import com.webinfotech.grocerszone.domain.interactors.base.AbstractInteractor;
import com.webinfotech.grocerszone.domain.models.User.UserInfo;
import com.webinfotech.grocerszone.domain.models.User.UserInfoWrapper;
import com.webinfotech.grocerszone.repository.User.UserRepositoryImpl;

public class CheckLoginInteractorImpl extends AbstractInteractor implements CheckLoginInteractor {

    UserRepositoryImpl mRepository;
    Callback mCallback;
    String email;
    String password;

    public CheckLoginInteractorImpl(Executor threadExecutor, MainThread mainThread, UserRepositoryImpl mRepository, Callback mCallback, String email, String password) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.email = email;
        this.password = password;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onLoginFail(errorMsg);
            }
        });
    }

    private void postMessage(UserInfo userInfo){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onLoginSuccess(userInfo);
            }
        });
    }

    @Override
    public void run() {
        final UserInfoWrapper userInfoWrapper = mRepository.logInUser(email, password);
        if (userInfoWrapper == null) {
            notifyError("Something Went Wrong");
        } else if (!userInfoWrapper.status) {
            notifyError(userInfoWrapper.message);
        } else {
            postMessage(userInfoWrapper.userInfo);
        }
    }
}
