package com.webinfotech.grocerszone.domain.interactors.impl;

import com.webinfotech.grocerszone.domain.executors.Executor;
import com.webinfotech.grocerszone.domain.executors.MainThread;
import com.webinfotech.grocerszone.domain.interactors.UpdateCartInteractor;
import com.webinfotech.grocerszone.domain.interactors.base.AbstractInteractor;
import com.webinfotech.grocerszone.domain.models.Cart.CartUpdateResponse;
import com.webinfotech.grocerszone.repository.ProductRepository.impl.CartRepositoryImpl;

/**
 * Created by Raj on 09-01-2019.
 */

public class UpdateCartInteractorImpl extends AbstractInteractor implements UpdateCartInteractor {

    CartRepositoryImpl mRepository;
    Callback mCallback;
    int quantity;
    int userId;
    int cartId;
    String apiKey;

    public UpdateCartInteractorImpl(Executor threadExecutor,
                                    MainThread mainThread,
                                    CartRepositoryImpl repository,
                                    Callback callback,
                                    int quantity,
                                    int userId,
                                    int cartId,
                                    String apiKey) {
        super(threadExecutor, mainThread);
        this.mRepository = repository;
        this.mCallback = callback;
        this.quantity = quantity;
        this.userId = userId;
        this.cartId = cartId;
        this.apiKey = apiKey;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUpdateCartFail(errorMsg);
            }
        });
    }

    private void postMessage(final String successMsg){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUpdateCartSuccess(successMsg);
            }
        });
    }

    @Override
    public void run() {
        final CartUpdateResponse updateResponse = mRepository.updateCart(userId, apiKey, cartId, quantity);
        if(updateResponse == null){
            notifyError("Something went wrong");
        }else if(!updateResponse.status){
            notifyError(updateResponse.message);
        } else{
            postMessage(updateResponse.message);
        }
    }
}
