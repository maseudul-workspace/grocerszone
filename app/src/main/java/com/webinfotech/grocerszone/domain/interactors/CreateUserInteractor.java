package com.webinfotech.grocerszone.domain.interactors;

public interface CreateUserInteractor {
    interface Callback {
        void onUserCreateSuccess();
        void onUserCreateFail(String errorMsg);
    }
}
