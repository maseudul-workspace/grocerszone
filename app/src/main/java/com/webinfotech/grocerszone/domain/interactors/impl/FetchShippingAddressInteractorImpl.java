package com.webinfotech.grocerszone.domain.interactors.impl;

import com.webinfotech.grocerszone.domain.executors.Executor;
import com.webinfotech.grocerszone.domain.executors.MainThread;
import com.webinfotech.grocerszone.domain.interactors.FetchShippingAddressInteractor;
import com.webinfotech.grocerszone.domain.interactors.base.AbstractInteractor;
import com.webinfotech.grocerszone.domain.models.User.ShippingAddress;
import com.webinfotech.grocerszone.domain.models.User.ShippingAddressWrapper;
import com.webinfotech.grocerszone.repository.User.UserRepositoryImpl;

public class FetchShippingAddressInteractorImpl extends AbstractInteractor implements FetchShippingAddressInteractor {

    UserRepositoryImpl mRepository;
    Callback mCallback;
    int userId;
    String apiToken;

    public FetchShippingAddressInteractorImpl(Executor threadExecutor, MainThread mainThread, UserRepositoryImpl mRepository, Callback mCallback, int userId, String apiToken) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.userId = userId;
        this.apiToken = apiToken;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddressFetchFail(errorMsg);
            }
        });
    }

    private void postMessage(ShippingAddress[] shippingAddresses){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddressFetchSuccess(shippingAddresses);
            }
        });
    }

    @Override
    public void run() {
        final ShippingAddressWrapper shippingAddressWrapper = mRepository.fetchShippingAddress(userId, apiToken);
        if (shippingAddressWrapper == null) {
            notifyError("Something Went Wrong");
        } else if (!shippingAddressWrapper.status) {
            notifyError(shippingAddressWrapper.message);
        } else {
            postMessage(shippingAddressWrapper.shippingAddresses);
        }
    }
}
