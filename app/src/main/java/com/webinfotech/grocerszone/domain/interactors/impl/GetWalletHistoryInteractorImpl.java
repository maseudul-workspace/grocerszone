package com.webinfotech.grocerszone.domain.interactors.impl;

import com.webinfotech.grocerszone.domain.executors.Executor;
import com.webinfotech.grocerszone.domain.executors.MainThread;
import com.webinfotech.grocerszone.domain.interactors.GetWalletHistoryInteractor;
import com.webinfotech.grocerszone.domain.interactors.base.AbstractInteractor;
import com.webinfotech.grocerszone.domain.models.Wallet.WalletDetails;
import com.webinfotech.grocerszone.domain.models.Wallet.WalletDetailsWrapper;
import com.webinfotech.grocerszone.repository.User.UserRepositoryImpl;

/**
 * Created by Raj on 22-02-2019.
 */

public class GetWalletHistoryInteractorImpl extends AbstractInteractor implements GetWalletHistoryInteractor {

    UserRepositoryImpl mRepository;
    Callback mCallback;
    String apiKey;
    int userId;
    int page;

    public GetWalletHistoryInteractorImpl(Executor threadExecutor,
                                          MainThread mainThread,
                                          Callback callback,
                                          UserRepositoryImpl repository,
                                          String apiKey,
                                          int userId,
                                          int page
                                          ) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.apiKey = apiKey;
        this.userId = userId;
        this.mRepository = repository;
        this.page = page;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingWalletHistoryFail(errorMsg);
            }
        });
    }

    private void postMessage(final WalletDetails walletDetails, int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingWalletHistorySuccess(walletDetails, totalPage);
            }
        });
    }

    @Override
    public void run() {
        final WalletDetailsWrapper walletDetailsWrapper = mRepository.getWalletHistory(apiKey, userId, page);
        if(walletDetailsWrapper == null){
            notifyError("Something went wrong !!! Please check your internet");
        }else if(!walletDetailsWrapper.status){
            notifyError(walletDetailsWrapper.message);
        }else{
            postMessage(walletDetailsWrapper.walletDetails, walletDetailsWrapper.totalPage);
        }
    }
}
