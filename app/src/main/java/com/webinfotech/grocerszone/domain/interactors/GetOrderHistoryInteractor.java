package com.webinfotech.grocerszone.domain.interactors;

import com.webinfotech.grocerszone.domain.models.Orders.OrderHistory;

/**
 * Created by Raj on 22-02-2019.
 */

public interface GetOrderHistoryInteractor {
    interface Callback{
        void onGettingOrderHistorySuccess(OrderHistory[] orderHistories);
        void onGettingOrderHistoryFail(String errorMsg);
    }
}
