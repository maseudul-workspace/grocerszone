package com.webinfotech.grocerszone.domain.interactors.impl;


import com.webinfotech.grocerszone.domain.executors.Executor;
import com.webinfotech.grocerszone.domain.executors.MainThread;
import com.webinfotech.grocerszone.domain.interactors.GetOrderHistoryInteractor;
import com.webinfotech.grocerszone.domain.interactors.base.AbstractInteractor;
import com.webinfotech.grocerszone.domain.models.Orders.OrderHistory;
import com.webinfotech.grocerszone.domain.models.Orders.OrderHistoryWrapper;
import com.webinfotech.grocerszone.repository.User.UserRepositoryImpl;

/**
 * Created by Raj on 22-02-2019.
 */

public class GetOrderHistoryInteractorImpl extends AbstractInteractor implements GetOrderHistoryInteractor {

    UserRepositoryImpl mRepository;
    Callback mCallback;
    String apiKey;
    int userId;

    public GetOrderHistoryInteractorImpl(Executor threadExecutor,
                                         MainThread mainThread,
                                         Callback callback,
                                         UserRepositoryImpl repository,
                                         String apiKey,
                                         int userId
                                         ) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.apiKey = apiKey;
        this.userId = userId;
        this.mRepository = repository;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingOrderHistoryFail(errorMsg);
            }
        });
    }

    private void postMessage(final OrderHistory[] orderHistories){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingOrderHistorySuccess(orderHistories);
            }
        });
    }


    @Override
    public void run() {
        final OrderHistoryWrapper orderHistoryWrapper = mRepository.getOrderHistory(apiKey, userId);
        if(orderHistoryWrapper == null){
            notifyError("Something went wrong !!! Please check your internet");
        }else if(!orderHistoryWrapper.status){
            notifyError(orderHistoryWrapper.message);
        }else{
            postMessage(orderHistoryWrapper.orderHistories);
        }
    }
}
