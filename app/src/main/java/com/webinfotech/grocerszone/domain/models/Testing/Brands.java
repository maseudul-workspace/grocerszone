package com.webinfotech.grocerszone.domain.models.Testing;

public class Brands {

    public String brandImage;
    public String brandName;

    public Brands(String brandImage, String brandName) {
        this.brandImage = brandImage;
        this.brandName = brandName;
    }

}
