package com.webinfotech.grocerszone.domain.interactors.impl;

import com.webinfotech.grocerszone.domain.executors.Executor;
import com.webinfotech.grocerszone.domain.executors.MainThread;
import com.webinfotech.grocerszone.domain.interactors.DeleteCartItemInteractor;
import com.webinfotech.grocerszone.domain.interactors.base.AbstractInteractor;
import com.webinfotech.grocerszone.domain.models.Cart.CartDeleteResponse;
import com.webinfotech.grocerszone.repository.ProductRepository.impl.CartRepositoryImpl;

/**
 * Created by Raj on 10-01-2019.
 */

public class DeleteCartItemInteractorimpl extends AbstractInteractor implements DeleteCartItemInteractor {

    CartRepositoryImpl mRepository;
    int userId;
    int cartId;
    String apiKey;
    Callback mCallback;

    public DeleteCartItemInteractorimpl(Executor threadExecutor,
                                        MainThread mainThread,
                                        CartRepositoryImpl repository,
                                        Callback callback,
                                        int userId,
                                        int cartId,
                                        String apiKey) {
        super(threadExecutor, mainThread);
        this.mRepository = repository;
        this.mCallback = callback;
        this.userId = userId;
        this.cartId = cartId;
        this.apiKey = apiKey;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCartItemDeleteFail(errorMsg);
            }
        });
    }

    private void postMessage(final String successMsg){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCartItemDeletedSuccess(successMsg);
            }
        });
    }

    @Override
    public void run() {
        final CartDeleteResponse cartDeleteResponse = mRepository.deleteCartItem(userId, apiKey, cartId);
        if(cartDeleteResponse == null){
            notifyError("Something went wrong");
        }else if(!cartDeleteResponse.status){
            notifyError(cartDeleteResponse.message);
        } else{
            postMessage(cartDeleteResponse.message);
        }
    }
}
