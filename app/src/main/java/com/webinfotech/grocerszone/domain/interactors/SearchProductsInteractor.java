package com.webinfotech.grocerszone.domain.interactors;

import com.webinfotech.grocerszone.domain.models.Products.Product;

/**
 * Created by Raj on 27-02-2019.
 */

public interface SearchProductsInteractor {
    interface Callback{
        void onGettingProductsSuccess(Product[] products, String searchKey, int totalPage);
        void onGettingProductsFail(String searchKey);
    }
}
