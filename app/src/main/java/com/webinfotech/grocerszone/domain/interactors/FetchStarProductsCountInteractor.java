package com.webinfotech.grocerszone.domain.interactors;

public interface FetchStarProductsCountInteractor {
    interface Callback {
        void onGettingStarProductsCountSuccess(int starProductsCount);
        void onGettingStarProductsCountFail(String errorMsg);
    }
}
