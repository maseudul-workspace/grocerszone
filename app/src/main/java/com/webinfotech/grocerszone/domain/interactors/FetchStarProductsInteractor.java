package com.webinfotech.grocerszone.domain.interactors;

import com.webinfotech.grocerszone.domain.models.Products.StarProduct;

public interface FetchStarProductsInteractor {
    interface Callback {
        void onGettingStarProductsSuccess(StarProduct[] starProducts, int totalPage);
        void onGettingStarProductsFail(String errorMsg);
    }
}
