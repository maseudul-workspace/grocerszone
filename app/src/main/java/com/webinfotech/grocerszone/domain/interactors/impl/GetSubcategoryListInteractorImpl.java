package com.webinfotech.grocerszone.domain.interactors.impl;

import com.webinfotech.grocerszone.domain.executors.Executor;
import com.webinfotech.grocerszone.domain.executors.MainThread;
import com.webinfotech.grocerszone.domain.interactors.GetSubcategoryListInteractor;
import com.webinfotech.grocerszone.domain.interactors.base.AbstractInteractor;
import com.webinfotech.grocerszone.domain.models.Category.SubCategory;
import com.webinfotech.grocerszone.domain.models.Category.SubcategoryWrapper;
import com.webinfotech.grocerszone.repository.Category.CategoryRepositoryImpl;

public class GetSubcategoryListInteractorImpl extends AbstractInteractor implements GetSubcategoryListInteractor {

    CategoryRepositoryImpl mRepository;
    Callback mCallback;
    int catId;

    public GetSubcategoryListInteractorImpl(Executor threadExecutor, MainThread mainThread, CategoryRepositoryImpl mRepository, Callback mCallback, int catId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.catId = catId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingSubcategoriesFail(errorMsg);
            }
        });
    }

    private void postMessage(SubCategory[] subcategories){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingSubcategoriesSuccess(subcategories);
            }
        });
    }

    @Override
    public void run() {
        final SubcategoryWrapper subcategoryWrapper = mRepository.getSubcategoryList(catId);
        if (subcategoryWrapper == null) {
            notifyError("Something Went Wrong");
        } else if (!subcategoryWrapper.status) {
            notifyError(subcategoryWrapper.message);
        } else {
            postMessage(subcategoryWrapper.subcategories);
        }
    }
}
