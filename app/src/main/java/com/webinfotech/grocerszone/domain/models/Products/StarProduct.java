package com.webinfotech.grocerszone.domain.models.Products;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StarProduct {

    @SerializedName("purchase_date")
    @Expose
    public String purchaseDate;

    @SerializedName("p_name")
    @Expose
    public String productName;

    @SerializedName("mrp")
    @Expose
    public String mrp;

    @SerializedName("price")
    @Expose
    public String price;

    @SerializedName("image")
    @Expose
    public String image;

}
