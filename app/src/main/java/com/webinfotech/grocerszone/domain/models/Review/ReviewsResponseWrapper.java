package com.webinfotech.grocerszone.domain.models.Review;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 28-02-2019.
 */

public class ReviewsResponseWrapper {
    @SerializedName("status")
    @Expose
    public Boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("code")
    @Expose
    public int code;

    @SerializedName("data")
    @Expose
    public Review[] reviews;

}
