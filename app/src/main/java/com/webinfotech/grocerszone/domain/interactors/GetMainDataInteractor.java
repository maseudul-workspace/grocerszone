package com.webinfotech.grocerszone.domain.interactors;

import com.webinfotech.grocerszone.domain.models.MainData;

public interface GetMainDataInteractor {
    interface Callback {
        void onGettingMainDataSuccess(MainData mainData);
        void onGettingMainDataFail(String errorMsg);
    }
}
