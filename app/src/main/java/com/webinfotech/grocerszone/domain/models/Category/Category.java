package com.webinfotech.grocerszone.domain.models.Category;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 11-02-2019.
 */

public class Category {
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("category")
    @Expose
    public String category;

    @SerializedName("image")
    @Expose
    public String image;

    @SerializedName("sub_category_status")
    @Expose
    public boolean subCategoryStatus;

}
