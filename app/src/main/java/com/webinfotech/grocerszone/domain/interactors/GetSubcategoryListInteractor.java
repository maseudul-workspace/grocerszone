package com.webinfotech.grocerszone.domain.interactors;


import com.webinfotech.grocerszone.domain.models.Category.SubCategory;

public interface GetSubcategoryListInteractor {
    interface Callback {
        void onGettingSubcategoriesSuccess(SubCategory[] subcategories);
        void onGettingSubcategoriesFail(String errorMsg);
    }
}
