package com.webinfotech.grocerszone.domain.interactors;

public interface AddAddressInteractor {
    interface Callback {
        void onAddressAddSuccess();
        void onAddAddressFail(String errorMsg);
    }
}
