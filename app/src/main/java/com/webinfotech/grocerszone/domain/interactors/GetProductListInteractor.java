package com.webinfotech.grocerszone.domain.interactors;

import com.webinfotech.grocerszone.domain.models.Products.Product;

public interface GetProductListInteractor {
    interface Callback {
        void onGettingProductListSuccess(Product[] products, int totalPage);
        void onGettingProductListFail(String errorMsg);
    }
}
