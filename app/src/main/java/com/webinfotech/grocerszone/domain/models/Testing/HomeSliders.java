package com.webinfotech.grocerszone.domain.models.Testing;

public class HomeSliders {

    public String imageUrl;
    public String name;

    public HomeSliders(String imageUrl, String name) {
        this.imageUrl = imageUrl;
        this.name = name;
    }
}
