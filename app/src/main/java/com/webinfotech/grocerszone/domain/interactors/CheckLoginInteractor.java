package com.webinfotech.grocerszone.domain.interactors;

import com.webinfotech.grocerszone.domain.models.User.UserInfo;

public interface CheckLoginInteractor {
    interface Callback {
        void onLoginSuccess(UserInfo userInfo);
        void onLoginFail(String errorMsg);
    }
}
