package com.webinfotech.grocerszone.domain.interactors;

import com.webinfotech.grocerszone.domain.models.Wallet.WalletDetails;

/**
 * Created by Raj on 22-02-2019.
 */

public interface GetWalletHistoryInteractor {

    interface Callback{
        void onGettingWalletHistorySuccess(WalletDetails walletDetails, int totalPage);
        void onGettingWalletHistoryFail(String errorMsg);
    }

}
