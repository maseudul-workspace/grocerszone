package com.webinfotech.grocerszone.domain.models.Wallet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 21-02-2019.
 */

public class WalletStatus {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("user_id")
    @Expose
    public int userId;

    @SerializedName("inactive_amount")
    @Expose
    public double inactiveAmount;

    @SerializedName("active_amount")
    @Expose
    public double activeAmount;

    @SerializedName("total_amount")
    @Expose
    public double totalAmount;

}
