package com.webinfotech.grocerszone.domain.models.Orders;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 22-02-2019.
 */

public class OrderProducts {
    @SerializedName("product_name")
    @Expose
    public String product_name;

    @SerializedName("product_image")
    @Expose
    public String product_image;

    @SerializedName("order_price")
    @Expose
    public String order_price;

    @SerializedName("order_quantity")
    @Expose
    public int order_quantity;
}
