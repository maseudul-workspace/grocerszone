package com.webinfotech.grocerszone.domain.models.Testing;

public class Products {

    public String productImage;
    public String productName;
    public String productPrice;

    public Products(String productImage, String productName, String productPrice) {
        this.productImage = productImage;
        this.productName = productName;
        this.productPrice = productPrice;
    }
}
